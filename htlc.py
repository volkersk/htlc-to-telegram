import json

from constants import HTLC_SOURCE_FILE, REMOVE_ALIAS_PREFIX


class OutcomeInfo(object):
    def __init__(
            self,
            incoming_timelock: int,
            outgoing_timelock: int,
            incoming_amt_msat: int,
            outgoing_amt_msat: int
    ):
        self.incoming_timelock = incoming_timelock
        self.outgoing_timelock = outgoing_timelock
        self.incoming_amt_msat = incoming_amt_msat
        self.outgoing_amt_msat = outgoing_amt_msat


class Channel(object):
    def __init__(
            self,
            alias: str,
            capacity: int,
            remote_balance: int,
            local_balance: int
    ):
        self.alias = alias.replace(REMOVE_ALIAS_PREFIX, '', 1)
        self.capacity = capacity
        self.remote_balance = remote_balance
        self.local_balance = local_balance

    def __eq__(self, other):
        if isinstance(other, Channel):
            return self.alias == other.alias
        return False


class Event(object):
    def __init__(
            self,
            incoming_channel: Channel,
            outgoing_channel: Channel,
            timestamp: int,
            event_type: str,
            event_outcome: str,
            event_outcome_info: OutcomeInfo,
            wire_failure: str,
            failure_detail: str,
            failure_string: str
    ):
        self.incoming_channel = incoming_channel
        self.outgoing_channel = outgoing_channel
        self.timestamp = timestamp
        self.event_type = event_type
        self.event_outcome = event_outcome
        self.event_outcome_info = event_outcome_info
        self.wire_failure = wire_failure
        self.failure_detail = failure_detail
        self.failure_string = failure_string


def parse_event(unparsed_event: str) -> Event:
    json_line = json.loads(unparsed_event)
    return Event(
        incoming_channel=Channel(
            json_line['incoming_channel'],
            json_line['incoming_channel_capacity'] if 'incoming_channel_capacity' in json_line else 0,
            json_line['incoming_channel_local_balance'] if 'incoming_channel_local_balance' in json_line else 0,
            json_line['incoming_channel_remote_balance'] if 'incoming_channel_remote_balance' in json_line else 0
        ) if 'incoming_channel' in json_line else None,
        outgoing_channel=Channel(
            json_line['outgoing_channel'],
            json_line['outgoing_channel_capacity'] if 'outgoing_channel_capacity' in json_line else 0,
            json_line['outgoing_channel_local_balance'] if 'outgoing_channel_local_balance' in json_line else 0,
            json_line['outgoing_channel_remote_balance'] if 'outgoing_channel_remote_balance' in json_line else 0
        ) if 'outgoing_channel' in json_line else None,
        timestamp=json_line['timestamp'],
        event_type=json_line['event_type'],
        event_outcome=json_line['event_outcome'],
        event_outcome_info=OutcomeInfo(
            incoming_timelock=json_line['event_outcome_info']['incoming_timelock']
            if 'incoming_timelock' in json_line['event_outcome_info']
            else 0,
            outgoing_timelock=json_line['event_outcome_info']['outgoing_timelock']
            if 'outgoing_timelock' in json_line['event_outcome_info']
            else 0,
            incoming_amt_msat=json_line['event_outcome_info']['incoming_amt_msat']
            if 'incoming_amt_msat' in json_line['event_outcome_info']
            else 0,
            outgoing_amt_msat=json_line['event_outcome_info']['outgoing_amt_msat']
            if 'outgoing_amt_msat' in json_line['event_outcome_info']
            else 0
        ) if 'event_outcome_info' in json_line else None,
        wire_failure=json_line['wire_failure'] if 'wire_failure' in json_line else '',
        failure_detail=json_line['failure_detail'] if 'failure_detail' in json_line else '',
        failure_string=json_line['failure_string'] if 'failure_string' in json_line else ''
    )


def get_events() -> ([Event], int):
    events = []
    parse_failures = 0
    with open(HTLC_SOURCE_FILE, "r+") as events_file:
        for line in events_file:
            try:
                event = parse_event(line)
                if event.event_type == 'FORWARD':
                    events.append(event)
            except Exception as e:
                parse_failures += 1
                print(repr(e), 'Unable to parse line, skipping: {0}'.format(line))
        events_file.truncate(0)
    return events, parse_failures
