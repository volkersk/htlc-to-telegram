from datetime import datetime
import pytz
import requests

from constants import TELEGRAM_BOT_KEY, TELEGRAM_CHAT_ID, TELEGRAM_ALIAS
from htlc import Event

utc = pytz.utc
amsterdam = pytz.timezone('Europe/Amsterdam')


def send_events(success_events: [Event], failure_events: [Event], parse_failures: int) -> int:
    alias_message = '<b>{0}</b>\n'.format(TELEGRAM_ALIAS)
    success_message = get_success_message(success_events, False)
    failure_message = get_failure_message(failure_events, False)
    parse_failures_message = get_parse_failures_message(parse_failures)
    message = combine_messages([alias_message, success_message, failure_message, parse_failures_message])

    if len(message) > 4096:
        if len(success_events) > len(failure_events):
            success_message = get_success_message(success_events, True)
        else:
            failure_message = get_failure_message(failure_events, True)
    message = combine_messages([alias_message, success_message, failure_message, parse_failures_message])

    return send_message(message)


def get_success_message(success_events: [Event], shorten: bool) -> str:
    message = ''
    if len(success_events) > 0:
        message += '<u>SUCCESS:</u> {0}x'.format(len(success_events))
        if shorten:
            message += '\n🟢 <i>Shortened, too many events</i>'
        else:
            for event in success_events:
                incoming_amt_sat = int(
                    event.event_outcome_info.incoming_amt_msat / 1000
                ) if event.event_outcome_info is not None else -1
                fee_msat = event.event_outcome_info.incoming_amt_msat - event.event_outcome_info.outgoing_amt_msat if event.event_outcome_info is not None else -1
                ppm = round(
                    fee_msat / event.event_outcome_info.incoming_amt_msat * 1_000_000
                ) if event.event_outcome_info is not None else -1
                message += '\n🟢 {0}\namount: {1:,} sat\nfee: {2:,} msat\nfee rate: {3:,} ppm\nincoming: <code>{4}</code>\noutgoing: <code>{5}</code>\noutcome: {6}\n'.format(
                    utc.localize(datetime.fromtimestamp(event.timestamp)).astimezone(amsterdam).strftime('%H:%M'),
                    incoming_amt_sat,
                    fee_msat,
                    ppm,
                    event.incoming_channel.alias,
                    event.outgoing_channel.alias,
                    event.event_outcome
                )
    return message


def get_failure_message(failure_events: [Event], shorten: bool) -> str:
    message = ''
    if len(failure_events) > 0:
        message += '<u>FAILURE:</u> {0}x'.format(len(failure_events))
        if shorten:
            message += '\n🔴 <i>Shortened, too many events</i>'
        else:
            for event in failure_events:
                message += '\n🔴 {0}\n'.format(
                    utc.localize(datetime.fromtimestamp(event.timestamp)).astimezone(amsterdam).strftime('%H:%M')
                )
                if event.event_outcome_info is not None:
                    message += 'amount: {0:,} sat\n'.format(
                        int(event.event_outcome_info.incoming_amt_msat / 1000)
                    )
                    if event.event_outcome != 'forward_fail_event':
                        message += 'max: {0:,} sat\n'.format(
                            event.outgoing_channel.remote_balance
                        )
                message += 'incoming: <code>{0}</code>\noutgoing: <code>{1}</code>\noutcome: {2}\n'.format(
                    event.incoming_channel.alias,
                    event.outgoing_channel.alias,
                    event.event_outcome
                )
                if event.wire_failure != '' or event.failure_detail != '' or event.failure_string != '':
                    message += 'extra info:\n'
                if event.wire_failure != '':
                    message += '- {0}\n'.format(event.wire_failure)
                if event.failure_detail != '':
                    message += '- {0}\n'.format(event.failure_detail)
                if event.failure_string != '':
                    message += '- {0}\n'.format(event.failure_string)
    return message


def get_parse_failures_message(parse_failures: int) -> str:
    if parse_failures > 0:
        return '⚠ <i>Failed to parse {0} HTLC events</i>'.format(parse_failures)
    else:
        return ''


def combine_messages(messages: [str]) -> str:
    return '\n'.join(
        list(
            filter(
                lambda m: (m != ''),
                messages
            )
        )
    )


def send_message(message: str) -> int:
    uri = 'https://api.telegram.org/bot{0}/sendMessage'.format(TELEGRAM_BOT_KEY)
    response = requests.post(uri, json={
        'chat_id': TELEGRAM_CHAT_ID,
        'text': message,
        'parse_mode': "HTML"
    })
    return response.status_code
