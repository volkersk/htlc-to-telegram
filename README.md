# htlc-to-telegram
## Installation
Clone the git repo:
```
git clone https://bitbucket.org/volkersk/htlc-to-telegram.git
```

Navigate to the folder:
```
cd htlc-to-telegram
```

Install requirements:
```
pip3 install -r requirements.txt
```

Configure *constants.py*:
```
nano constants.py
```

Configure crontab:
```
crontab -e
0 * * * * python3 /home/umbrel/htlc-to-telegram/htlc-to-telegram.py > /dev/null 2>&1
```
