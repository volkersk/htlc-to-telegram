from constants import HTLC_SHOW_SUCCESS, HTLC_SHOW_FAILURE
from htlc import get_events
from telegram import send_events

if __name__ == '__main__':
    events = []
    parse_failures = 0
    try:
        print('Getting HTLC events...')
        events, parse_failures = get_events()
        print('Finished getting HTLC events ({0})'.format(len(events)))
    except Exception as e:
        print(repr(e), 'Unable to get HTLC events')
        quit()

    success_events = []
    failure_events = []
    skip_next = False
    for index, event in enumerate(events):
        if skip_next:
            skip_next = False
            continue
        if event.event_outcome == 'forward_event':
            next_index = index + 1
            if next_index < len(events):
                next_event = events[next_index]
                if event.timestamp > next_event.timestamp - 60 and event.incoming_channel == next_event.incoming_channel and event.outgoing_channel == next_event.outgoing_channel:
                    event.event_outcome = next_event.event_outcome
                    event.event_outcome_info = event.event_outcome_info if next_event.event_outcome_info is None else next_event.event_outcome_info
                    event.failure_detail = next_event.failure_detail
                    event.failure_string = next_event.failure_string
                    if event.event_outcome == 'settle_event':
                        if HTLC_SHOW_SUCCESS:
                            success_events.append(event)
                    else:
                        if HTLC_SHOW_FAILURE:
                            failure_events.append(event)
                    skip_next = True
            continue

        if event.event_outcome == 'settle_event':
            if HTLC_SHOW_SUCCESS:
                success_events.append(event)
        else:
            if HTLC_SHOW_FAILURE:
                failure_events.append(event)

    if len(success_events) == 0 and len(failure_events) == 0 and parse_failures == 0:
        print('No HTLC events, stopping...')
        quit()

    try:
        print('Sending telegram message...')
        status = send_events(success_events, failure_events, parse_failures)
        print('Finished sending telegram message [{0}]'.format(status))
    except Exception as e:
        print(repr(e), "Unable to send telegram message")
        quit()
